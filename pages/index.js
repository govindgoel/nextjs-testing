import Link from 'next/link'

export default function Home() {
  return (
    <nav>
      <Link href="/about">
        <a style={{
          width: "50%",
          marginLeft: "50%",
          marginRight: "50%",
          marginTop: "25%",
        }}>About</a>
      </Link>
    </nav>
  )
}
